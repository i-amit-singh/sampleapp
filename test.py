import unittest
from calfunction import add, sub, mul, div

class TestCalFunction(unittest.TestCase):

    def test_add(self):
        result = add(3, 5)
        self.assertEqual(result, 8)

    def test_sub(self):
        result = sub(10, 3)
        self.assertEqual(result, 7)

    def test_mul(self):
        result = mul(4, 6)
        self.assertEqual(result, 24)

    def test_div(self):
        result = div(15, 3)
        self.assertEqual(result, 5)

    def test_divide_by_zero(self):
        result = div(10, 0)
        self.assertEqual(result, 'Cannot divide by zero')

if __name__ == '__main__':
    unittest.main()
