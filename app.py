from flask import Flask, render_template, request
from calfunction import *
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def calculator():
    result = None

    if request.method == 'POST':
        num1 = float(request.form['num1'])
        num2 = float(request.form['num2'])
        operation = request.form['operation']

        if operation == 'add':
            result = add(num1,num2)
        elif operation == 'subtract':
            result = sub(num1,num2)
        elif operation == 'multiply':
            result = mul(num1,num2)
        elif operation == 'divide':
            result = div(num1,num2)

    return render_template('calculator.html', result=result)

if __name__ == '__main__':
    app.run(debug=True)
