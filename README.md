# SimpleApp
- Simple App has a simple web based calculator that can perform simple addition, substraction, multiplication and division.
- It is based on Flask (python module)
- Image will be provided in "screenshot" folder

# General steps to run SimpleApp 
- install required libraries from requirement.txt file
- test the calculator function file (calfunction.py) using test cases (unittest).
- run app.py
- open http://127.0.0.1:5000 on local browser to access SimpleApp

#  CI/CD pipeline for Gitlab (.gitlab-ci.yml)
- image used is python with latest version
- pipeline consists of 3 stages/jobs
    - build job: install required libraries from requirement.txt file and pass the module using artifact
    - test job: test the calculator functions using test cases (unittest)
    - deploy job: run the app.py and exit

# CI/CD pipeline for Jenkins (Jenkinsfile) - **For Windows System**
- select agent (any)
- define environment paths
    - WINDOWS COMMAND PROMPT (CMD)
    - PYTHON
- **Stages** 
    1. Checkout: Checkout the source code from the GitLab repository
        Generate script with gitlab repository link and credentials (access token) in SampleApp>Configurations>Pipeline Syntax>Sample Step:Git.
    2. Build: Build the standalone application
        Set the predefined environment path & install python modules using pip and requirement.txt
    3. Test: Execute any basic tests for the application
        Test the calculator function file (calfunction.py) using test cases (unittest).
    4. Deploy: Deploy the application to a test environment
        Run app.py file and open http://127.0.0.1:5000 on local browser to access SimpleApp

# GitLab CI to trigger the Jenkins pipeline
1. Install Gitlab pluggin in Jenkins
2. Configure Gitlab plugin in Manage Jenkins>System>GitLab and add Gitlab API token
3. Test connection to receive success message
4. In GitLab project go to Settings>Integration>Jenkins and select Configure
5. Add Jenkins url and user id and password and select and save
6. Check by committing changes and the jenkins pipeline will trigger
